import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

function Square(props) {
    return (
        <button
            className={props.isWinning? 'square winning':'square'}
            onClick={props.onClick}>
            {props.value}
        </button>
    );
}

class Board extends React.Component {
    renderSquare(i) {
        const winning = this.props.winning && this.props.winning.indexOf(i)!== -1;
        return <Square key={i}
            value={this.props.squares[i]}
            onClick={() => this.props.onClick(i)}
            isWinning={winning}
        />;
    }

    render() {
        let rows = [];
        for(let i = 0; i < 3; i++) {
            let cols = [];
            for(let j = 0; j < 3; j++) {
                cols.push(this.renderSquare(i * 3 + j));
            }
            rows.push(<div key={'board_row' + i} className="board-row">{cols}</div>)
        }
        return (
            <div>
                {rows}
            </div>
        );
    }
}

class Game extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            history: [{
                squares: Array(9).fill(null),
            }],
            stepNumber: 0,
            xIsNext: true,
            moved: false,
            order: 'asc',
        }
    }
    handleClick(i) {
        const history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();
        if (calculateWinner(squares) || squares[i]) {
            return;
        }
        squares[i] = this.state.xIsNext ? 'X' : 'O';
        this.setState({
            history: history.concat([{
                squares: squares,
                col: Math.floor(i / 3),
                row: i % 3,
            }]),
            stepNumber: history.length,
            xIsNext: !this.state.xIsNext,
            moved: false,
        });
    }

    getNewOrder() {
        return this.state.order === 'asc' ? 'desc' : 'asc'
    }

    handleSort() {
        this.setState({
            order: this.getNewOrder(),
        })

    }

    jumpTo(step){
        this.setState({
            stepNumber: step,
            xIsNext: (step % 2) === 0,
            moved: true,
        })
    }

    render() {
        let history = this.state.history.slice();
        let moveOffset = 0;
        const current = history[this.state.stepNumber];
        const winner = calculateWinner(current.squares);
        if(this.state.order === 'desc') {
            history = history.reverse();
            moveOffset = history.length - 1;
        }

        const moves = history.map((step, move) => {
            const moveNumber = moveOffset + (this.state.order === 'desc' ? -move : move);
            const desc = moveNumber ?
                'Go to move #' + moveNumber :
                'Go to game start';
            return (
                <li key={moveNumber}>
                    <button className={this.state.moved && this.state.stepNumber === moveNumber ? 'active' : ''} onClick={() => this.jumpTo(moveNumber)}>{desc}</button>
                    {moveNumber > 0 &&
                    <span>
                    col: {step.col}, row: {step.row}
                    </span>
                    }
                </li>
            )
        });

        let status;
        if (winner) {
            status = 'Winner: '+ winner.mark;
        }
        else if (moves.length < 9){
            status = 'Next player: ' + (this.state.xIsNext ? 'X' : 'O');
        }
        else{
            status = 'Result: draw';
        }
        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        squares={current.squares}
                        winning={winner ? winner.squares : null}
                        onClick={(i) => this.handleClick(i)}
                    />
                </div>
                <div className="game-info">
                    <div>{status}</div>
                    <button onClick={() => this.handleSort()}>Sort {this.getNewOrder()}</button>
                    <ol>{moves}</ol>
                </div>
            </div>
        );
    }
}

// ========================================

ReactDOM.render(
    <Game/>,
    document.getElementById('root')
);
function calculateWinner(squares) {
    const lines = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ];
    for (let i = 0; i < lines.length; i++) {
        const [a, b, c] = lines[i];
        if(squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
            return {mark: squares[a], squares: lines[i]};
        }
    }
    return null;
}